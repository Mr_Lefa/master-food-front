import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

import JwPagination from 'jw-vue-pagination';
import 'core-js'

import VueScrollFixedNavbar from "vue-scroll-fixed-navbar";
import VueSimpleAlert from "vue-simple-alert";

Vue.use(VueSimpleAlert);
Vue.use(VueScrollFixedNavbar);
Vue.component('jw-pagination', JwPagination);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
