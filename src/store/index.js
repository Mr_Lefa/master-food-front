import Vue from 'vue'
import Vuex from 'vuex'
// import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    search: "",
    infoPage: [],
    cartItems: [],
    userIfos: {
      email: "",
      password: ""
    },
    commandes: [],
    items: [
      {
        id: 0,
        img: 'https://png.pngtree.com/templates/20180809/restaurant-logos-png-png_25706.jpg',
        title: 'RESTAURANT  ',
        price: 10000,
        color: 'yellow',
        description: 'RESTAURANT ABIDJAN',
        category: 'Moteur',
        marque: 'BMW'
      },
      {
        id: 0,
        img: 'https://lh3.googleusercontent.com/proxy/XHYDwdBRTvsrmAbiEChmhtmjHfBk2t7rIaWa7vZlY7KI0M1g0riBjrNJTFMcS2NOMrcAfVjugkF6eU5PG0-DsSlHaMH2omYFdT2Jg3K0YnE7A-Noegg3ySf6a6mBh16dBg',
        title: 'RESTAURANT  ',
        price: 10000,
        color: 'yellow',
        description: 'RESTAURANT ABIDJAN',
        category: 'Moteur',
        marque: 'BMW'
      },
      {
        id: 0,
        img: 'https://img.cuisineaz.com/680x357/2016/09/23/i51334-recettes-africaines.jpg',
        title: 'RESTAURANT  ',
        price: 10000,
        color: 'yellow',
        description: 'RESTAURANT ABIDJAN',
        category: 'Moteur',
        marque: 'BMW'
      },
      {
        id: 0,
        img: 'https://img.freepik.com/vecteurs-libre/pack-logo-restaurant-retro_23-2148369916.jpg?size=338&ext=jpg',
        title: 'RESTAURANT  ',
        price: 10000,
        color: 'yellow',
        description: 'RESTAURANT ABIDJAN',
        category: 'Moteur',
        marque: 'BMW'
      },
      {
        id: 0,
        img: 'https://png.pngtree.com/templates/20180809/restaurant-logos-png-png_25706.jpg',
        title: 'RESTAURANT  ',
        price: 10000,
        color: 'yellow',
        description: 'RESTAURANT ABIDJAN',
        category: 'Moteur',
        marque: 'BMW'
      },
      {
        id: 0,
        img: 'https://365psd.com/images/previews/469/restaurant-logo-with-chef-35068.jpg',
        title: 'RESTAURANT  ',
        price: 10000,
        color: 'yellow',
        description: 'RESTAURANT ABIDJAN',
        category: 'Moteur',
        marque: 'BMW'
      },



    ],
    products: [
      {
        name: "pneu",
        price: 44,
        description: 'Pneu kit complet',
        marque: "TOYOTA",
        category: 'Pneumatique',
        img: 'https://img-4.linternaute.com/B7A5cDw9FKrmKE8gKnkh961tNHw=/350x/smart/26b79281d0a14950af2307e69008d66f/ccmcms-linternaute/1432728.jpg'

      },
      {
        name: "radiateur",
        price: 20,
        marque: "TOYOTA",
        description: 'Radiateur pour nissan',
        category: 'Chauffage/Ventilation',
        img: 'https://s.alicdn.com/@sc04/kf/H4f02302921424d6eaaf84a4b32918b33o.jpg_300x300.jpg'
      },
      {
        name: "huile",
        price: 2500,
        marque: "TOYOTA",
        description: 'Huile a moteur',
        category: 'Moteur',
        img: 'https://images.lteplatform.com/images/products/600x600/521776031.jpg?v=11.03'
      },
      {
        name: "frein",
        price: 6,
        marque: "MERCEDES",
        description: 'frein',
        category: 'Moteur',
        img: 'https://www.eurocarparts.com/catalogimages/categories/102x136/ukcp2_09.jpg'
      },
      {
        name: "bras",
        price: 5,
        marque: "MERCEDESS",
        description: 'Bras de suspension complet',
        category: 'Direction',
        img: 'https://www.my-procar.com/thumbs/data/categories/visuels/54921774e565f99bc05a8e1c89a23950[800,600,1,0,0,0,0,0,0,0,0,0,90].jpg'
      },
      {
        name: "tambour",
        price: 44,
        marque: "MERCEDESS",
        description: 'Tambour de frein arriere et avant',
        category: 'Freinage',
        img: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRgWFRYYGRgYHB4cGhwaHBgcHhkeHBocGiEfHBoeIy4lHB8rIR4aJjgnKzAxNTU1HCQ7QDs0Py40NTEBDAwMEA8QGBESGjEhGCExMTQ0NDE0NDE0MTQ0NDExMTQxMTQ0NDQxMTExPzE0MTQ0NDE0NDQ/NDQ/NDQ0NDExMf/AABEIAMIBAwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAUDBgcCAf/EAD8QAAIBAgIIAwQHBwQDAQAAAAECAAMRBCEFBhIxQVFhcSKBkTKhscETQlJictHwBzOCkqKy4RQjwvFDU3Mk/8QAFwEBAQEBAAAAAAAAAAAAAAAAAAECA//EAB0RAQEBAAIDAQEAAAAAAAAAAAABEQIxEiFRcUH/2gAMAwEAAhEDEQA/AOzREQEREBERAREQEREBERA+RPl7SHW0pRXfUXyN/hGmJt4lFX1noruu3oPjn7pX1tbj9RB5kn8pm8o1ONbbE08a4sDnTU/xEfIyQmuK/WpMOzA/ECPKHjW0RNeTWygd61F7hfk0m4TTuHqGyuAeTeG/a+Rl2Jl+LWIiVCIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiJCx+NSku02/go3k/rjAlkym0hrBTS4XxHneyjz4+U1PTGsLMSCwsOAyUd/tHvNN0jrIoNk8bc/qjz4+U53lb06TjJ23HSen2e5ZvCPJR5cfOapjtZkBsgLnney+Rtn5Ca/fEYlrAM9uAHhXvwHnL/V7Ut67sj1EQqASo8bWNxfI239YnH6XlnStqaw1m9kIvkWPvy90iVcfWf2qj+R2fhadEp/syp/WruewUfIyTR/Z1hgbM9RsrjxAX57hwy9RL4p5OTtSvmd/OfRtD2XYdiR8J2NP2f4Ib0du7v8jJSalYEf8AhU9yx+JlxNcUGMrDdVf+ZvnM2H0/XT2mFReTWB8mHzvO2pqtghuw1L+RT8pJoaIwwHhoUhwyReGXKPE1zXQmvxSy7ZT7tTxJ5NwHmJvujdcaT2FQbBP1h4lPpmPf3lPrXqXhHQ1UpCmy5tseAEcTsjw3G+9uE0FtA1aB26FW6A5o/LjuyJ9Iyzo2Xt3xHBAIIIOYIzBHQz3OY6u6xPQGywZkOeywYBTzV7EDtNrwut1B+DfwlWA9D8pZUrZIkbCYtai7Sm49CO44STKhERAREQEREBERAREQEREBERAT5EgaUxwpJtb2OSjmfyEluLJr7pLSK0lzzY7hz6nkJzbT2niSxL5/Wbgo5LyEwaf06W2iXy+sx49B04Sq0Pod8UwqVQVoA3RNxqdTyX9dZj3y/HT1x/XnRmjnxh2m2kw4PY1LcByXr8917X1RwzMGVCtvqgnYPcbx5GXtOmFAAAAAsAMgAOUiYrFEtsUxdz6KPtNyHxmpMYt1AdBTGwoCgbgosPKRtXsaUxyHg42D5mw+PukrHIiJsXLuTdnPA9uA5DhNdp1CuJpH76/Ef4lR2cGfKm9T1t6gj429J5R55rvkPxL/AHCVEiJ4+kmNqwHGBnmNDm3f5CYGxS/aHrMK45PEdob/AIAfO8DLpL904te6sD5gzl+IrjZsTvE3jTWlQKbhd7AgeeU1vVjRCVQ9SqoezlEU5hdkC5K7iTfjINXTFVE9h2tyve3rJ1HS5a22iOPvKL+smax6NSlUAQBVZb7PI3Iy5D8pr9Snsm49IV0HRuP2bVKByGTJxHQjiOnpNy0ZpNay3GTcR8xzE4vo7SLIwZDnx5Ecjzm4aPxwfx0jsuM2W/vH57jxgdIiVuh9IfSpnk65MPnaWUqEREBERAREQEREBERAREQPDMALnICc61s0xtE2O/Jei8T3P63TZdaNJhFKA8Lt0HAdz+t85PpSu9aotNPbqm34F4nsBf3zFu3HTjMms2g9HDFVS7i9GkbAcHfrzA/LmZvagAdpG0bgko01poPCot3PEnqTnMeNqszLTQ2ZuP2VG9vkOpEsmM2684nFM7fR07FuLH2UHNuvIcffKbWLSn+kp7FHN3Nnc5kG28825DcLdLSfi8WlFfoqXtfWbeQTvJPFj7pUOiOLOAwO8HO8qKvVnGPULq7Ftmx2ibm5vkTx3XmfSAWnVpO5sofM9rN8pOw9ahTGypRRyBUZ/GRdL0hiQlOmbttE7jbJTxtA2RteKCjOoPIE/ASJX/aBRuLFyAb5KRwsBnbnfymtYTU6s7MOABF+G0bWHU7z2Ek0dUSNgsfZqbDjntMoHucekqLCt+0JT7KOe5UfOQK2vlQ+zSA7t/iZhqmqI5LZ0nDA8wVVreoPrLTC6rYYVKiORsqVYbsi22CPRR6QKehrJXckF6aWFz4XYgd7gfGQMTp+sBZaw8gsy6R0fh6eIZKZ2gWYEXv4SL+dvlLfRuEwKoC9JmfO91PM239IELVrFPUR3qOWIYC54ZcOUt8FpM0GYI6Wc3KMQfFa1xY3vkPSQsZp3AUjnhjfqgA9d0aM0gH2mTCXV2JU3yUZC2yvUH1kH3G4hqjF3N2PLcOgHKUmmKmyjEbyLDzymxY2nUe3+2lMC/Jb35kmU+O0cHFmcGxBsue7ruhVFo3FXFjvEu8HiWRgymxHGQRoxFNwufO5nuk0Do2hNK+zWXePDUUfr08uU3ynUDAEG4IuDOLaHx30b3PsNk46c/KdL1bxZBNIm4ttIenEfP1hGxREShERAREQEREBERA+Ty7AAk8M56lbp7EbFFvveH13+68luRZNuOaa1aUuzljkLu3nmB5C08anaNIU4moPHUHhB+onAeeR7WlNpP8A36qU/wD21Bf8Cnab3Cb+AAJnjP61yv8AGPFVgilidwlJjsW1FLi309bcCwBRB35X/mbpJzuHcs37ulmeTOMwOtt/e0o8TVNRy5GZyHRRuE0yjUsFUO96K93uf6QZYYLDqi2erSY3OYp1H8uAmOnTEzqoAubAczaBmo4qmDa1Ww4rSpIuXAbTM3unnEaYpq6f7O2ASSXcsVyIyQWW/DsTKbSGmB7NNdrm27+UH4zBQfaF5UbFV1uKjwU8wLC9hboAMh5SnfWCpdb2ttGo/V9oFV7DZTPoZWYzEBe/AStZ3fp2gWw0rUOyGYnx7b/ePhAHYBfeZmw6O4v9KWJcu+8bVxYXtmLXPrKSno5jvJkylouopujsCOsDYaGi6DOtSpt7SiwC7Nj3JJ3XPCe9J6Xw9DIUS7HcGcm/cKBIuCxdRfDUX+ID4j8pT6aJOJBJuCt077j84EyvrLVZfBRpUz91EY2/iBnrB6w1GVA7swdtkkNs7JvbMDK27lKioZgwfiKJbLbBPG924AZwN6fCH7JkBqyFtgMNrlxkrE6be5VUNwbeyd9wOPHMDzmvVy5rJU2CLsFsMydrkB0PvkwTcQkq3IDkX/xLHHu+y2wvitlcjf2v8bSgwuGqJcvxNznc3hV1SM3rVzG+Gk/2DsN23f22nP8ADtNm1brZun2htDuP0PSB12JD0XX26SNxtY9xkfhJkqEREBERAREQEREBNP16xmygUHgT5t4R6ANNwnNf2gVbVtnhYMf5R/mZ5dNce2t6s0dvFu/CimyPxPmf6QR5zatJViiEjfuA5k5AetpUak4fZw+2R4qzs57E2X3AHzlniBt1UTgCXPZN39RU+UQvaBpqyU0pKd+bHnbeT3Yk+Up6tYIpY3IHK3HKS9K19usxG5Tsjy3++8p9OPZFHNvcB/kSomaH0i1Z2UJsoguTe5JJyHIcfSR9PI/0i3J2NnIcNq+d/L5zHqliFAqKciWB75S+x+HV0N+AuOhiI1UiPpyim285AdZKbB8zK1yGY23DIfnKPqU7m5zJllhcLMGEpTe9W9XQ6irVyp8BuL2+C/GBB0Lq69bxKAqj6zbvLnNlo6v4akNqo+1brsr6D8580jpbLYo2VFyuLcOAHKUJxYdtlNuq44IC5Hc7l8yIGzbeCP1Kf9A+ciYrQ+j6y5oBmbOpNlYZe0CQPPKVjYesPaw1UD8Ib+0kzBRIuXpvstuJW2duDqcmtyIy6SCl1l1OrUAzoDUpWvtLcso5su+3UXHaUep+FD19ps0pjbPI2IAF/O/lOkaM0wyeI5KhtWQeyFb2aqA5gb7jo28i51XW3RX+hd3oj/axLApbcjWLFO2ZKjllwlEnH1aIJYuR0UKo3g33DO4HpNW0tjVc2pbXYEnmN/YmSqOjRs7dclm32JyHfrMYrn6iKq8ydm/aBOL3AJFrgXHKQMVuMzIzEXKkDru8juMw4jcZFecPLbRNQrVQj7QHkcj8ZUYeWeBPjT8S/EQOt6ufu2HJz8FMuJTauew4+9/xWXMqEREBERAREQEREBOYftF/fn8HynT5y79o379vwD4TPLprj2y6vZYWh/8AJP7BPKVtkVqx4eBf4fzZrfwz5odrYOj0pJ/YJB0u+xh6KD6/iY8zbaP9TX8pUVKNzlRp+rd0UcFJ9T/iWQe0jYjFIpzAJ4ZXMCo0dU2S18r2+c2rRGN2wylr2687yvpYkb2p2Xnsiw723SzWijC4GyeDLYEefHscoRDxL2DdL+6U2Hl7jsKxQk2JzBIyv1tw6yjwolGy6uaMNaqqDcc2PJRvPyHUibzp3EgBaKZKoFwOQFgP185C1CohaFSpbxFtkHoqg/FvdKvSVZihKnxuyqDyLsFB8r+6BL0TotsWxzK4dG2WsSGqEb1B+qg3EjM5gTZNYnTC4KoUsll2U2fDZj4Ra3Eb/KS6JoYOgqsy00RbDaIBNum9mJzyzJM5D+0DXH/VsKdMEU0JtzY7rnrwtwuecixV6C0/ixVNsTXORNmqO4FiD7LEi3S06HovGjGKwZUTFKLhlGz9KBwPX4dric41Ywpu1Q7rbK9cwT8APWbtoNClek6jPbVT2bw/AmVEhn2Hpvu8QR+qOwQg+ZB7iWWHTbwVek/iNBzsXz2VUhlt2G0O0r9Ppb6UL9she5fL32ltoTxVMYD7LAbXfZYZe/0gcx1lp1GdLKSgzNsxtXO+3S0maOoKoZ2AZURSq8CW4/zbRPlymw4nRrBcyD8TKOpXXDsVKFhUGyVByPXkDc++QWlOm9vG5Yn2hls58BlfLvNcxO4y5xWL8OyoI4XO+0pcSdw6wFASwwreJe4+MhURLPRdLaqIPvC/lmfhCuq6uey/4/8AiJdSs0ElqQP2iT77D3CWcqEREBERAREQERED5Ob/ALSqBFRWA9umR3Kk/IrOkTVtfsHt4cOBnTYH+FvCfeVPlM2el4321jR+WCpf/JP7BK/WH2MOPuH+1JK0bUvhSPsbSeQzX+krIus+6gPuH4JKKRzlIOBwpqVmu2yFGZ+yvTqTl59JPC3mbAUlDODltrs3+F/U+sCVhApF0o3XcGZvEw+0Lw+BKVFdGtT2CqpncNtAm54jdaWNNDYXFsgOnLKesemwhY2sgJ7kwjDTAsb8ec13HYT6OpcewxuDy5iQmD1Ttu5scwFysO/ylnopNu9F2Zgc1J3qeh4/9yjour6FcAtt77Z8yzW+AlFVcKaTn2Uq02bsHGflv8pdanO3+mei/tUmuPwt4gR3IaVWLoKS9Nhkbi266texB5W49DykGna9YLEnHVx4nXaBXxL7LKGAtfcL28pU4LQDkg1Dsr9kG5PS+4TpWIwgxaKruqYqn4Vc5LXThf73Mc72yOVeuhMSjbL0XNuKDaB8xlKK7C4cKAFAAGQA5TadA4cKTUb2KY2j1P1QOt55wehWAD1LU05vkx7LvvPWktIIECINlAchvZ23XPM8h+gEHEPtVEB+39I/QKdrP+Kwlzq8v/5qrnfUc27fotKPC4R3cUhk9Ug1OOwg3KfLM9TLM6XRqz0KX7rDIEFtzOT4ieezskd9rnA848jdy3zRsd48UBwT5Z/lNux+ICKzMcgLmafo07TvUPE2Hnn+UCTiDKuobt2k3FPvkKmLyKk0hL3V1P8Ac2vsqT57vmZSoJuOpuALG/22H8q5k/KB0TB0tlEXkoHumeIlQiIgIiICIiAiIgJGxuGFSm9M7mUr6i15JiByDRoZVr0yLMOHUXVvkJ41nOVBhuKGx/lMvNYcAaGM+kt4KxNj95hZgeu1Y+fSVWlqW1hjzov/AE/lssP5ZmfGr9UNOe8TTJQ7NwQL5dOEwUXk+k0qNeIuLGblonEDE0GR83UbD/eBGTefxBmp4mnsOy8L3HY5iZsDinpttI2ybWvkbjkQd8zLjVmpmJ0e6Egq1huIBsfOW2g9FOG23Fhbwg78+NuGUwUNY6o9pUb1U+ouPdJTazC2VLPq2Xwzl8onjW06MrCm5Yi4YbLdr387Z+pmPTOCVFB2gKf/AI33imT9Rz9g5WPDIbwNrT6usNZt2wvYXPqfym1ag4utXFZKjB0UL7Sg5vfIcNmwuRb4x5Sl42RArhkH+4nh+2vjQ/xDcPxART0wAABXYAbgHNh75s+L1aRQWou1A7zsG6eaHIdxaVf+ifjpCn/R+c0yqmxLv4lVm+/UJVf5mzPkDPGFpu77NO1SqfrgeCmvHZv/AHf9TNj6mAoeLE4v6dt4SmQxPcKSB3JWa9pT9oDlGp4SgMOhy2rg1CO4yU9fEeREC309p2lgEejhyHxTgh6m8U777fe6cN55Sv1MTZou3NgPRQf+U0GoWP6JvNooaTKUFpU/bYkm3C5t62AkVK07jDVcUUOQPiPX8hDKEUKu4e/vPGCwv0akt7bb+nSYMVWgR8Q9zaeqSzxTSSUWBIw9IswUb2IA851bVfBBU2uFtlew3nzPwmiaraPZn2rfdT8R3nyHxnVaFIKoUbgAB5QjLERKEREBERAREQEREBERAr9L6PWvSam2V81birDcR+t15pD4Yh3pOLbaEMOAIyNudw1/KdHmp604Qq6VgMgRfvYg+q/CQcpUFGKNvU2PllJtJ5L1m0fsv9IuavnfrvI89/8A1KmjVhWfSOELgMntKLEcx06yoV5e0as84/ACp4lyf3N369ZLFlVaVZ7+kkStSdDZ1I+fY7jPIqzONammpMuF0vXo3+hrvTBzIUixPMggiVT158Rb5makS1YaT03icQoWviKlRR9Xwqp/EEADecrw1sglh0t8Ja4NMvCV6qw3+cwV6N2IC2J+qM/S0uM6hCqDunh3ljiNAYgI1X6Fwii7EqRlzscyO086v6M/1DNnZV3m1znwHXfJi6r6GHZ2AUEk8BmTNqwGjRSXabN/cvQfnLvDYBKS2RQOZ4nuZX6XfYW545SorsXiLStuSbz6zFjczMiQFNJYYDCl3CDjvPIcSZHROAF+gm/6o6Dtmw5FviE+ZgXurmjBTQNa2VlHJefc75exEqEREBERAREQEREBERAREQEwYmgrqVbcRM8QOfY7A2L4epuPsnlxFviPMTQsfg2puynIqfX/ABOz6e0d9Kl19pcxzPG3zH+ZouldH/TKCLComRG7aH63ecitOpVue+TqNeQ8ThiCQQQRvHETCrsOsDYkIYWYAg8CAR6SNiNXab5oSh9R6HMSJg8aLgE27zZsOuUDWn1RbhUU9wR8CZVVcE9JzTe1wAQRuIPEe/0nRNnKVWA0YlfSASpuFMPs/aCuwI7XIv3gS9WNFYFgquRUq72BY7I6AA2a3ObvhsJTpi1NEQfdVV+An3E06KJdwiItiMgoU7hbkbm2XOVjayUbeAPU/ChUbr73tfK+6+4yoi4mnVxNZ6au1OkmTuLXYncq39byvr6rU8Ld6TOQ5AdWsfEAx2gQBbqLS80ZXrM7lcNZX9pi43r4czbpYgC8mtoypVYGsVVRuRSSb8SW3e74yK1/B4Bqh2VF+Z4DuZsNLVmhsFaiBywsSeH4fs998uKFFUAVQABwEyyo4zrFq4+Fe2bU2vsP8m5MJW06dyAASTO24/BJWQ06i7Sn3dQeBE1PRmqJp1Tc3F8m5L0H2uHT4xULVjVwkhm9rieCdBzb4fHfqFFUAVRYD9Z8zPtCiFUKosBMsqEREBERAREQEREBERAREQEREBERASn0rodanjXwvz4Hv+cuIgc30ro0OdiouxUG5uf5j9CajjsA9NtlxbkeBHMHjO14/AJVXZcdjxE1nHaCqAWKLVTh9oeRzv2kVy1qM6LqtoxMRQV1qEOvhcFQbMOxGRFj5ynxWgEJOwxpt9l93rvHvk/U76TDYjYdTsVbLcZqHHsnpe5XzEDYDq2321I7Ee65mBdVqhqI5rFAn2Dmb2zzXlcW6zbolRT1NBq3t1Kj2IIBKgCxB9lVAztbsTM+H0PQQWVButdrsdxG9r8CZYxA+AT7EQEREBERAREQEREBERAREQEREBERAREQEREBERAREQERECt0xSU0iSASBkSBcec1zQX71e/yMRIN1iIlCIiAiIgIiICIiAiIgIiICIiAiIgIiIH/2Q=='

      },
      {
        name: "pare-choc",
        price: 20,
        marque: "BMW",
        description: 'Pare-choc avant et arrierre BMW',
        category: 'Carrosserie',
        img: 'https://www.tizautoparts.com/wp-content/uploads/2019/12/bmw_F82_M4_TIZAUTOPARTS_BUMPERS_SPARE_PARTS.jpg'
      },
      {
        name: "filtre",
        price: 34500,
        marque: "BMW",
        description: 'Filtre d"abiclage climatisseur',
        category: 'Climatisation',
        img: 'https://www.vroomly.com/media/thumbnails/interventions/WXK9OmDiKCVS_700x700_mtdhGWCw.jpg'
      },
      {
        name: "compresseur",
        price: 20000,
        marque: "TOYOTA",
        description: 'Compresseur de climatisation R15',
        category: 'Climatisation',
        img: 'https://sc04.alicdn.com/kf/HTB1DV1LXOLrK1Rjy1zdq6ynnpXaY.jpg'

      },
      {
        name: "silencieux",
        price: 23500,
        marque: "BMW",
        description: 'Silencieux',
        category: 'Echappement',
        img: 'https://www.generation-tuning.fr/172/silencieux-universel-inox-2x63-pour-vehicule-toute-marque.jpg'
      },
      {
        name: "Pot",
        price: 750000,
        marque: "TOYOTA",
        description: 'Pot Catalytique unversel ',
        category: 'Echappement',
        img: 'https://i.ebayimg.com/images/g/078AAMXQVT9TCIRF/s-l300.jpgg'
      },
      {
        name: "demarreur",
        price: 82000,
        marque: "BMW",
        description: 'Demarreur BMW',
        category: 'Moteur',
        img: 'https://www.best4moto.com/115175-large_default/demarreur-tecnium.jpg'
      },
      {
        name: "bougie",
        price: 20000,
        marque: "TOYOTA",
        description: 'Bougie d"alumage moteur a combustion',
        category: 'Moteur',
        img: 'https://e7.pngegg.com/pngimages/788/141/png-clipart-car-auto-expo-spark-plug-motorcycle-internal-combustion-engine-car-cleaning-transport.png'
      },
      {
        name: "culasse",
        price: 2,
        marque: "BMW",
        description: 'culasse automobile',
        category: 'Moteur',
        img: 'https://www.vroomly.com/media/images/iStock-813984680.width-500.jpg'
      },
      {
        name: "verre",
        price: 3000,
        marque: "BMW",
        description: "Kit de verre ",
        category: 'Eclairage',
        img: 'https://images-na.ssl-images-amazon.com/images/I/61VduVhySqL._AC_SY450_.jpg'
      },
      {
        name: "pneu",
        price: 44,
        marque: "BMW",
        description: 'Pneu kit complet',
        category: 'Pneumatique',
        img: 'https://img-4.linternaute.com/B7A5cDw9FKrmKE8gKnkh961tNHw=/350x/smart/26b79281d0a14950af2307e69008d66f/ccmcms-linternaute/1432728.jpg'

      },
      {
        name: "radiateur",
        price: 20,
        marque: "MERCEDES",
        description: 'Radiateur pour nissan',
        category: 'Chauffage/Ventilation',
        img: 'https://s.alicdn.com/@sc04/kf/H4f02302921424d6eaaf84a4b32918b33o.jpg_300x300.jpg'
      },
      {
        name: "huile",
        price: 2500,
        marque: "TOYOTA",
        description: 'Huile a moteur',
        category: 'Moteur',
        img: 'https://images.lteplatform.com/images/products/600x600/521776031.jpg?v=11.03'
      },
      {
        name: "frein",
        price: 6,
        marque: "BMW",
        description: 'frein',
        category: 'Moteur',
        img: 'https://www.eurocarparts.com/catalogimages/categories/102x136/ukcp2_09.jpg'
      },
      {
        name: "bras",
        price: 5,
        marque: "MERCEDES",
        description: 'Bras de suspension complet',
        category: 'Direction',
        img: 'https://www.my-procar.com/thumbs/data/categories/visuels/54921774e565f99bc05a8e1c89a23950[800,600,1,0,0,0,0,0,0,0,0,0,90].jpg'
      },
      {
        name: "tambour",
        price: 44,
        marque: "TOYOTA",
        description: 'Tambour de frein arriere et avant',
        category: 'Freinage',
        img: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRgWFRYYGRgYHB4cGhwaHBgcHhkeHBocGiEfHBoeIy4lHB8rIR4aJjgnKzAxNTU1HCQ7QDs0Py40NTEBDAwMEA8QGBESGjEhGCExMTQ0NDE0NDE0MTQ0NDExMTQxMTQ0NDQxMTExPzE0MTQ0NDE0NDQ/NDQ/NDQ0NDExMf/AABEIAMIBAwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAUDBgcCAf/EAD8QAAIBAgIIAwQHBwQDAQAAAAECAAMRBCEFBhIxQVFhcSKBkTKhscETQlJictHwBzOCkqKy4RQjwvFDU3Mk/8QAFwEBAQEBAAAAAAAAAAAAAAAAAAECA//EAB0RAQEBAAIDAQEAAAAAAAAAAAABEQIxEiFRcUH/2gAMAwEAAhEDEQA/AOzREQEREBERAREQEREBERA+RPl7SHW0pRXfUXyN/hGmJt4lFX1noruu3oPjn7pX1tbj9RB5kn8pm8o1ONbbE08a4sDnTU/xEfIyQmuK/WpMOzA/ECPKHjW0RNeTWygd61F7hfk0m4TTuHqGyuAeTeG/a+Rl2Jl+LWIiVCIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiJCx+NSku02/go3k/rjAlkym0hrBTS4XxHneyjz4+U1PTGsLMSCwsOAyUd/tHvNN0jrIoNk8bc/qjz4+U53lb06TjJ23HSen2e5ZvCPJR5cfOapjtZkBsgLnney+Rtn5Ca/fEYlrAM9uAHhXvwHnL/V7Ut67sj1EQqASo8bWNxfI239YnH6XlnStqaw1m9kIvkWPvy90iVcfWf2qj+R2fhadEp/syp/WruewUfIyTR/Z1hgbM9RsrjxAX57hwy9RL4p5OTtSvmd/OfRtD2XYdiR8J2NP2f4Ib0du7v8jJSalYEf8AhU9yx+JlxNcUGMrDdVf+ZvnM2H0/XT2mFReTWB8mHzvO2pqtghuw1L+RT8pJoaIwwHhoUhwyReGXKPE1zXQmvxSy7ZT7tTxJ5NwHmJvujdcaT2FQbBP1h4lPpmPf3lPrXqXhHQ1UpCmy5tseAEcTsjw3G+9uE0FtA1aB26FW6A5o/LjuyJ9Iyzo2Xt3xHBAIIIOYIzBHQz3OY6u6xPQGywZkOeywYBTzV7EDtNrwut1B+DfwlWA9D8pZUrZIkbCYtai7Sm49CO44STKhERAREQEREBERAREQEREBERAT5EgaUxwpJtb2OSjmfyEluLJr7pLSK0lzzY7hz6nkJzbT2niSxL5/Wbgo5LyEwaf06W2iXy+sx49B04Sq0Pod8UwqVQVoA3RNxqdTyX9dZj3y/HT1x/XnRmjnxh2m2kw4PY1LcByXr8917X1RwzMGVCtvqgnYPcbx5GXtOmFAAAAAsAMgAOUiYrFEtsUxdz6KPtNyHxmpMYt1AdBTGwoCgbgosPKRtXsaUxyHg42D5mw+PukrHIiJsXLuTdnPA9uA5DhNdp1CuJpH76/Ef4lR2cGfKm9T1t6gj429J5R55rvkPxL/AHCVEiJ4+kmNqwHGBnmNDm3f5CYGxS/aHrMK45PEdob/AIAfO8DLpL904te6sD5gzl+IrjZsTvE3jTWlQKbhd7AgeeU1vVjRCVQ9SqoezlEU5hdkC5K7iTfjINXTFVE9h2tyve3rJ1HS5a22iOPvKL+smax6NSlUAQBVZb7PI3Iy5D8pr9Snsm49IV0HRuP2bVKByGTJxHQjiOnpNy0ZpNay3GTcR8xzE4vo7SLIwZDnx5Ecjzm4aPxwfx0jsuM2W/vH57jxgdIiVuh9IfSpnk65MPnaWUqEREBERAREQEREBERAREQPDMALnICc61s0xtE2O/Jei8T3P63TZdaNJhFKA8Lt0HAdz+t85PpSu9aotNPbqm34F4nsBf3zFu3HTjMms2g9HDFVS7i9GkbAcHfrzA/LmZvagAdpG0bgko01poPCot3PEnqTnMeNqszLTQ2ZuP2VG9vkOpEsmM2684nFM7fR07FuLH2UHNuvIcffKbWLSn+kp7FHN3Nnc5kG28825DcLdLSfi8WlFfoqXtfWbeQTvJPFj7pUOiOLOAwO8HO8qKvVnGPULq7Ftmx2ibm5vkTx3XmfSAWnVpO5sofM9rN8pOw9ahTGypRRyBUZ/GRdL0hiQlOmbttE7jbJTxtA2RteKCjOoPIE/ASJX/aBRuLFyAb5KRwsBnbnfymtYTU6s7MOABF+G0bWHU7z2Ek0dUSNgsfZqbDjntMoHucekqLCt+0JT7KOe5UfOQK2vlQ+zSA7t/iZhqmqI5LZ0nDA8wVVreoPrLTC6rYYVKiORsqVYbsi22CPRR6QKehrJXckF6aWFz4XYgd7gfGQMTp+sBZaw8gsy6R0fh6eIZKZ2gWYEXv4SL+dvlLfRuEwKoC9JmfO91PM239IELVrFPUR3qOWIYC54ZcOUt8FpM0GYI6Wc3KMQfFa1xY3vkPSQsZp3AUjnhjfqgA9d0aM0gH2mTCXV2JU3yUZC2yvUH1kH3G4hqjF3N2PLcOgHKUmmKmyjEbyLDzymxY2nUe3+2lMC/Jb35kmU+O0cHFmcGxBsue7ruhVFo3FXFjvEu8HiWRgymxHGQRoxFNwufO5nuk0Do2hNK+zWXePDUUfr08uU3ynUDAEG4IuDOLaHx30b3PsNk46c/KdL1bxZBNIm4ttIenEfP1hGxREShERAREQEREBERA+Ty7AAk8M56lbp7EbFFvveH13+68luRZNuOaa1aUuzljkLu3nmB5C08anaNIU4moPHUHhB+onAeeR7WlNpP8A36qU/wD21Bf8Cnab3Cb+AAJnjP61yv8AGPFVgilidwlJjsW1FLi309bcCwBRB35X/mbpJzuHcs37ulmeTOMwOtt/e0o8TVNRy5GZyHRRuE0yjUsFUO96K93uf6QZYYLDqi2erSY3OYp1H8uAmOnTEzqoAubAczaBmo4qmDa1Ww4rSpIuXAbTM3unnEaYpq6f7O2ASSXcsVyIyQWW/DsTKbSGmB7NNdrm27+UH4zBQfaF5UbFV1uKjwU8wLC9hboAMh5SnfWCpdb2ttGo/V9oFV7DZTPoZWYzEBe/AStZ3fp2gWw0rUOyGYnx7b/ePhAHYBfeZmw6O4v9KWJcu+8bVxYXtmLXPrKSno5jvJkylouopujsCOsDYaGi6DOtSpt7SiwC7Nj3JJ3XPCe9J6Xw9DIUS7HcGcm/cKBIuCxdRfDUX+ID4j8pT6aJOJBJuCt077j84EyvrLVZfBRpUz91EY2/iBnrB6w1GVA7swdtkkNs7JvbMDK27lKioZgwfiKJbLbBPG924AZwN6fCH7JkBqyFtgMNrlxkrE6be5VUNwbeyd9wOPHMDzmvVy5rJU2CLsFsMydrkB0PvkwTcQkq3IDkX/xLHHu+y2wvitlcjf2v8bSgwuGqJcvxNznc3hV1SM3rVzG+Gk/2DsN23f22nP8ADtNm1brZun2htDuP0PSB12JD0XX26SNxtY9xkfhJkqEREBERAREQEREBNP16xmygUHgT5t4R6ANNwnNf2gVbVtnhYMf5R/mZ5dNce2t6s0dvFu/CimyPxPmf6QR5zatJViiEjfuA5k5AetpUak4fZw+2R4qzs57E2X3AHzlniBt1UTgCXPZN39RU+UQvaBpqyU0pKd+bHnbeT3Yk+Up6tYIpY3IHK3HKS9K19usxG5Tsjy3++8p9OPZFHNvcB/kSomaH0i1Z2UJsoguTe5JJyHIcfSR9PI/0i3J2NnIcNq+d/L5zHqliFAqKciWB75S+x+HV0N+AuOhiI1UiPpyim285AdZKbB8zK1yGY23DIfnKPqU7m5zJllhcLMGEpTe9W9XQ6irVyp8BuL2+C/GBB0Lq69bxKAqj6zbvLnNlo6v4akNqo+1brsr6D8580jpbLYo2VFyuLcOAHKUJxYdtlNuq44IC5Hc7l8yIGzbeCP1Kf9A+ciYrQ+j6y5oBmbOpNlYZe0CQPPKVjYesPaw1UD8Ib+0kzBRIuXpvstuJW2duDqcmtyIy6SCl1l1OrUAzoDUpWvtLcso5su+3UXHaUep+FD19ps0pjbPI2IAF/O/lOkaM0wyeI5KhtWQeyFb2aqA5gb7jo28i51XW3RX+hd3oj/axLApbcjWLFO2ZKjllwlEnH1aIJYuR0UKo3g33DO4HpNW0tjVc2pbXYEnmN/YmSqOjRs7dclm32JyHfrMYrn6iKq8ydm/aBOL3AJFrgXHKQMVuMzIzEXKkDru8juMw4jcZFecPLbRNQrVQj7QHkcj8ZUYeWeBPjT8S/EQOt6ufu2HJz8FMuJTauew4+9/xWXMqEREBERAREQEREBOYftF/fn8HynT5y79o379vwD4TPLprj2y6vZYWh/8AJP7BPKVtkVqx4eBf4fzZrfwz5odrYOj0pJ/YJB0u+xh6KD6/iY8zbaP9TX8pUVKNzlRp+rd0UcFJ9T/iWQe0jYjFIpzAJ4ZXMCo0dU2S18r2+c2rRGN2wylr2687yvpYkb2p2Xnsiw723SzWijC4GyeDLYEefHscoRDxL2DdL+6U2Hl7jsKxQk2JzBIyv1tw6yjwolGy6uaMNaqqDcc2PJRvPyHUibzp3EgBaKZKoFwOQFgP185C1CohaFSpbxFtkHoqg/FvdKvSVZihKnxuyqDyLsFB8r+6BL0TotsWxzK4dG2WsSGqEb1B+qg3EjM5gTZNYnTC4KoUsll2U2fDZj4Ra3Eb/KS6JoYOgqsy00RbDaIBNum9mJzyzJM5D+0DXH/VsKdMEU0JtzY7rnrwtwuecixV6C0/ixVNsTXORNmqO4FiD7LEi3S06HovGjGKwZUTFKLhlGz9KBwPX4dric41Ywpu1Q7rbK9cwT8APWbtoNClek6jPbVT2bw/AmVEhn2Hpvu8QR+qOwQg+ZB7iWWHTbwVek/iNBzsXz2VUhlt2G0O0r9Ppb6UL9she5fL32ltoTxVMYD7LAbXfZYZe/0gcx1lp1GdLKSgzNsxtXO+3S0maOoKoZ2AZURSq8CW4/zbRPlymw4nRrBcyD8TKOpXXDsVKFhUGyVByPXkDc++QWlOm9vG5Yn2hls58BlfLvNcxO4y5xWL8OyoI4XO+0pcSdw6wFASwwreJe4+MhURLPRdLaqIPvC/lmfhCuq6uey/4/8AiJdSs0ElqQP2iT77D3CWcqEREBERAREQERED5Ob/ALSqBFRWA9umR3Kk/IrOkTVtfsHt4cOBnTYH+FvCfeVPlM2el4321jR+WCpf/JP7BK/WH2MOPuH+1JK0bUvhSPsbSeQzX+krIus+6gPuH4JKKRzlIOBwpqVmu2yFGZ+yvTqTl59JPC3mbAUlDODltrs3+F/U+sCVhApF0o3XcGZvEw+0Lw+BKVFdGtT2CqpncNtAm54jdaWNNDYXFsgOnLKesemwhY2sgJ7kwjDTAsb8ec13HYT6OpcewxuDy5iQmD1Ttu5scwFysO/ylnopNu9F2Zgc1J3qeh4/9yjour6FcAtt77Z8yzW+AlFVcKaTn2Uq02bsHGflv8pdanO3+mei/tUmuPwt4gR3IaVWLoKS9Nhkbi266texB5W49DykGna9YLEnHVx4nXaBXxL7LKGAtfcL28pU4LQDkg1Dsr9kG5PS+4TpWIwgxaKruqYqn4Vc5LXThf73Mc72yOVeuhMSjbL0XNuKDaB8xlKK7C4cKAFAAGQA5TadA4cKTUb2KY2j1P1QOt55wehWAD1LU05vkx7LvvPWktIIECINlAchvZ23XPM8h+gEHEPtVEB+39I/QKdrP+Kwlzq8v/5qrnfUc27fotKPC4R3cUhk9Ug1OOwg3KfLM9TLM6XRqz0KX7rDIEFtzOT4ieezskd9rnA848jdy3zRsd48UBwT5Z/lNux+ICKzMcgLmafo07TvUPE2Hnn+UCTiDKuobt2k3FPvkKmLyKk0hL3V1P8Ac2vsqT57vmZSoJuOpuALG/22H8q5k/KB0TB0tlEXkoHumeIlQiIgIiICIiAiIgJGxuGFSm9M7mUr6i15JiByDRoZVr0yLMOHUXVvkJ41nOVBhuKGx/lMvNYcAaGM+kt4KxNj95hZgeu1Y+fSVWlqW1hjzov/AE/lssP5ZmfGr9UNOe8TTJQ7NwQL5dOEwUXk+k0qNeIuLGblonEDE0GR83UbD/eBGTefxBmp4mnsOy8L3HY5iZsDinpttI2ybWvkbjkQd8zLjVmpmJ0e6Egq1huIBsfOW2g9FOG23Fhbwg78+NuGUwUNY6o9pUb1U+ouPdJTazC2VLPq2Xwzl8onjW06MrCm5Yi4YbLdr387Z+pmPTOCVFB2gKf/AI33imT9Rz9g5WPDIbwNrT6usNZt2wvYXPqfym1ag4utXFZKjB0UL7Sg5vfIcNmwuRb4x5Sl42RArhkH+4nh+2vjQ/xDcPxART0wAABXYAbgHNh75s+L1aRQWou1A7zsG6eaHIdxaVf+ifjpCn/R+c0yqmxLv4lVm+/UJVf5mzPkDPGFpu77NO1SqfrgeCmvHZv/AHf9TNj6mAoeLE4v6dt4SmQxPcKSB3JWa9pT9oDlGp4SgMOhy2rg1CO4yU9fEeREC309p2lgEejhyHxTgh6m8U777fe6cN55Sv1MTZou3NgPRQf+U0GoWP6JvNooaTKUFpU/bYkm3C5t62AkVK07jDVcUUOQPiPX8hDKEUKu4e/vPGCwv0akt7bb+nSYMVWgR8Q9zaeqSzxTSSUWBIw9IswUb2IA851bVfBBU2uFtlew3nzPwmiaraPZn2rfdT8R3nyHxnVaFIKoUbgAB5QjLERKEREBERAREQEREBERAr9L6PWvSam2V81birDcR+t15pD4Yh3pOLbaEMOAIyNudw1/KdHmp604Qq6VgMgRfvYg+q/CQcpUFGKNvU2PllJtJ5L1m0fsv9IuavnfrvI89/8A1KmjVhWfSOELgMntKLEcx06yoV5e0as84/ACp4lyf3N369ZLFlVaVZ7+kkStSdDZ1I+fY7jPIqzONammpMuF0vXo3+hrvTBzIUixPMggiVT158Rb5makS1YaT03icQoWviKlRR9Xwqp/EEADecrw1sglh0t8Ja4NMvCV6qw3+cwV6N2IC2J+qM/S0uM6hCqDunh3ljiNAYgI1X6Fwii7EqRlzscyO086v6M/1DNnZV3m1znwHXfJi6r6GHZ2AUEk8BmTNqwGjRSXabN/cvQfnLvDYBKS2RQOZ4nuZX6XfYW545SorsXiLStuSbz6zFjczMiQFNJYYDCl3CDjvPIcSZHROAF+gm/6o6Dtmw5FviE+ZgXurmjBTQNa2VlHJefc75exEqEREBERAREQEREBERAREQEwYmgrqVbcRM8QOfY7A2L4epuPsnlxFviPMTQsfg2puynIqfX/ABOz6e0d9Kl19pcxzPG3zH+ZouldH/TKCLComRG7aH63ecitOpVue+TqNeQ8ThiCQQQRvHETCrsOsDYkIYWYAg8CAR6SNiNXab5oSh9R6HMSJg8aLgE27zZsOuUDWn1RbhUU9wR8CZVVcE9JzTe1wAQRuIPEe/0nRNnKVWA0YlfSASpuFMPs/aCuwI7XIv3gS9WNFYFgquRUq72BY7I6AA2a3ObvhsJTpi1NEQfdVV+An3E06KJdwiItiMgoU7hbkbm2XOVjayUbeAPU/ChUbr73tfK+6+4yoi4mnVxNZ6au1OkmTuLXYncq39byvr6rU8Ld6TOQ5AdWsfEAx2gQBbqLS80ZXrM7lcNZX9pi43r4czbpYgC8mtoypVYGsVVRuRSSb8SW3e74yK1/B4Bqh2VF+Z4DuZsNLVmhsFaiBywsSeH4fs998uKFFUAVQABwEyyo4zrFq4+Fe2bU2vsP8m5MJW06dyAASTO24/BJWQ06i7Sn3dQeBE1PRmqJp1Tc3F8m5L0H2uHT4xULVjVwkhm9rieCdBzb4fHfqFFUAVRYD9Z8zPtCiFUKosBMsqEREBERAREQEREBERAREQEREBERASn0rodanjXwvz4Hv+cuIgc30ro0OdiouxUG5uf5j9CajjsA9NtlxbkeBHMHjO14/AJVXZcdjxE1nHaCqAWKLVTh9oeRzv2kVy1qM6LqtoxMRQV1qEOvhcFQbMOxGRFj5ynxWgEJOwxpt9l93rvHvk/U76TDYjYdTsVbLcZqHHsnpe5XzEDYDq2321I7Ee65mBdVqhqI5rFAn2Dmb2zzXlcW6zbolRT1NBq3t1Kj2IIBKgCxB9lVAztbsTM+H0PQQWVButdrsdxG9r8CZYxA+AT7EQEREBERAREQEREBERAREQEREBERAREQEREBERAREQERECt0xSU0iSASBkSBcec1zQX71e/yMRIN1iIlCIiAiIgIiICIiAiIgIiICIiAiIgIiIH/2Q=='

      },


    ],
  },
  // plugins: [createPersistedState()],
  getters: {
    itemsNumber(state) { // Cart Component
      return state.cartItems.length
    },
    totalPrice(state) { // Cart Component
      if (state.cartItems.length != 0) {
        return state.cartItems.reduce((a, b) => {
          return (b.price == null) ? a : a + b.price
        }, 0)
      }
    },
    infoLength(state) { // Info Component
      if (state.infoPage.length > 0) {
        return state.infoPage.splice(0, 1)
      }
    }
  },
  mutations: {
    inCart(state, n) { // Cart Component
      return state.cartItems.push(n)
    },
    outCart(state, n) { // Cart Component
      const index = state.cartItems.findIndex(x => x.id === n)
      return state.cartItems.splice(index, 1)
    },
    getCommandes(state, items) { // Cart Component
      state.commandes = items
      return state.commandes
    },

    getUser(state, user) { // Cart Component
      state.userIfos = user
      return state.userIfos
    },

    addtoInfo(state, n) { // Info Component
      return state.infoPage.push(n)
    }
  }
})
