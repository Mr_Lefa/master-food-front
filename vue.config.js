module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/master-food-front/dist' : '/',
  transpileDependencies: [
    'vuetify'
  ],
  chainWebpack: config => {
    config.module.rules.delete('eslint')
  }

}