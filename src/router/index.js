import Vue from 'vue'
import VueRouter from 'vue-router'

//const Home = () => import('@/views/Home.vue')
const Plats = () => import('@/views/Plats.vue')
const Contact = () => import('@/views/Contact.vue')
const Info = () => import('@/views/Info.vue')
const Accueil = () => import('@/views/Accueil.vue')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'accueil',
    component: Accueil
  },

  {
    path: '/plats',
    name: 'Plats',
    component: Plats
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/info',
    name: 'Info',
    component: Info
  }


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
